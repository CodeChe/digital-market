import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';

// import assets
import arrow from "../assets/svg/icon/arrow_right.svg";
import phoneImg from "../assets/products/galaxy-s20-plus.png";
import laptopImg from "../assets/products/DELL.png";
import accessoryImg from "../assets/products/headset.png";

// import style
import '../style/home.scss';
import '../style/logo.scss';

class Home extends Component<any, any>{

    // The type of market that are in the project
    marketType = {
        phone: {market: "فروشگاه موبایل", imgUrl: phoneImg, imgAlt: "phone picture(galaxy s20 plus)", order: 1},
        laptop: {market: "فروشگاه لپ تاپ", imgUrl: laptopImg, imgAlt: "laptop picture(DELL)", order: 2},
        accessory: {market: "فروشگاه لوازم جانبی", imgUrl: accessoryImg, imgAlt: "accessory picture(headset)", order:3}
    };

    // active in state means which market's information should be shown
    state = {
        active: this.marketType.phone,
        order: this.marketType.phone.order,
    };

    active = (type : object) => this.state.active === type ? "active" : "";
    // function to show arrow or not
    visibleArrow = (type : object) => this.state.active === type ? "visible" : "invisible";

    // function to return a list of markets and which one of them is active
    listOfTypes() {
        return (
            <ul>
                <div id="animationEl"> </div>
                <li onClick={() => this.selectionType(this.marketType.phone, 1)} className={this.active(this.marketType.phone)}>
                    <img src={arrow} alt="arrow icon" className={`arrow ${this.visibleArrow(this.marketType.phone)}`}/>
                    <p>موبایل</p>
                </li>
                <li onClick={() => this.selectionType(this.marketType.laptop, 2)} className={this.active(this.marketType.laptop)}>
                    <img src={arrow} alt="arrow icon" className={`arrow ${this.visibleArrow(this.marketType.laptop)}`}/>
                    <p>لپ تاپ</p>
                </li>
                <li onClick={() => this.selectionType(this.marketType.accessory, 3)} className={this.active(this.marketType.accessory)}>
                    <img src={arrow} alt="arrow icon" className={`arrow ${this.visibleArrow(this.marketType.accessory)}`}/>
                    <p>لوازم جانبی</p>
                </li>
            </ul>
        )
    }

    // function for selecting image of market that is active and put in slider
    slider() {
        return (
            <div className="slide">
                <img src={this.state.active.imgUrl} id="img-product" alt={this.state.active.imgAlt} />
                <Button className="market-btn" variant="info">{this.state.active.market}</Button>
            </div>
        )
    }

    // this function is called when one of the markets is clicked
    selectionType(type : object, orderDes : number){
        this.animation(this.state.order, orderDes);
        setTimeout(()=>{
            this.setState({
                active : type,
                order: orderDes
            });
        }, 60);
    }

    //  all of this function is to implementation animation when clicked one of the markets
    animation(origin:number, destination:number) {
        let dev = document.getElementById("animationEl")!;
        let li = document.querySelectorAll("li")[origin-1];
        let arrow = document.querySelectorAll(".arrow")[origin-1];
        li.classList.remove("active");

        // Do not display the current market text arrow
        arrow.classList.add("invisible");
        let top= getComputedStyle(dev).top;
        let width = getComputedStyle(dev).width;
        console.log(top);
        console.log(`width: ${width}`);
        // variable top is string e.g "12px" that first two letter is number
        let valueOfTop = parseInt(top.slice(0,2));
        let valueOfWidth:number;
        if (origin === 1){
            // in this condition value of width is two digits
            valueOfWidth = parseInt(width.slice(0,2));
        }
        else {
            // in this condition value of width is three digits
            valueOfWidth = parseInt(width.slice(0,3));
        }

        // find proper coefficient to add suitable amount to top and width of style
        let diff = destination - origin ;
        let counterInterval = 0;

        // this is a loop to increase top and width at equal time for show animation
        let animate = setInterval(()=>{
            if (counterInterval < 6 ) {
                valueOfTop = valueOfTop + (diff*5);
                valueOfWidth = valueOfWidth + (diff*1.5);
                top = `${valueOfTop}px`;
                width = `${valueOfWidth}px`;
                dev.style.top = top;
                dev.style.width = width;
                counterInterval +=1;
            } else {
                dev.style.top = `${(destination-1)*3}rem`;
                clearInterval(animate)
            }
        },10)

    }

    render() {
        return (
                <>
                    <div className="header">
                        <div id="user-situation">
                            <p>ورود</p>
                            <span id="line"> </span>
                            <p>ثبت نام</p>
                        </div>
                        <div id="logo-container">
                            <p id="logo">digital market</p>
                        </div>
                    </div>

                    {/*color title*/}
                    <div className="persian-title">
                        <p id="market-text">فروشگاه</p>
                        <p id="digital-text">دیجیتال</p>
                    </div>

                    <div className="content">
                        <div className="types">
                            <h2 className="mb-4 mr-3">انتخاب نوع محصول</h2>
                            {this.listOfTypes()}
                        </div>
                        <div className="slider">
                            {this.slider()}
                        </div>
                    </div>
                </>
        );
    }
}

export default Home;