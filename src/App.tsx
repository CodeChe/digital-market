import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

// import style
import './App.css';
import './style/bootstrap/css/bootstrap.min.css';

// import components
import Home from "./components/home";

function App() {
  return (
    <div className="App">
        <Router>
            <Switch>
                <Route path="/" exact component={Home}/>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
